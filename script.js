let timeout, btn = $('button'), ifr = $('iframe'), inp = $('input[type="search"]');

btn.click(function() {
   let url = 'https://api.wheretheiss.at/v1/satellites/25544';
   fetch(url)
   .then(response => response.json())
   .then(d => {
        let txt;
        if(d.error)
            txt = `Error: <b>${d.error}</b><br>
                    Status: <b>${d.status}</b>`;
        else {
            updateLoc(d.latitude+','+d.longitude);
            txt = `ID: <b>${d.id}</b><br>
                    Location: <b>${d.latitude.toFixed(2)}, ${d.longitude.toFixed(2)}</b><br>
                    Altitude: <b>${d.altitude.toFixed(2)} Km</b><br>
                    Velocity: <b>${d.velocity.toFixed(2)} Km/h</b><br>
                    Time: <b>${getIST(d.timestamp)}</b><br>
                    Visibility: <b>${d.visibility}</b><br>
                    Footprint: <b>${d.footprint}</b><br>
                    Daynum: <b>${d.daynum}</b>`;
        }
        $('#fill').html(txt);
   })
})

$('#auto').change(function() {
    get();
})

$('#map').change(function() {
    $('iframe').toggle();
})

function updateLoc(loc) {
    ifr.attr('src', `https://maps.google.com/maps?hl=en&q=${loc} &t=&z=2&ie=UTF8&iwloc=B&output=embed`)
}

function get() {
    if($('#auto').is(':checked'))
        timeout = setInterval(() => {
            btn.click();
        }, 2500)
    else
        clearInterval(timeout);
}

function getIST(t) {
    return new Date(new Date(t * 1000).getTime()).toLocaleString();
}

